// Access the database

const Task = require("../Models/task.js");

/*Controllers and functions*/

// Controller/function to get all the task on our database
module.exports.getAll = (request, response)=> {

	Task.find({})
	// to capture the result of the find method
	.then(result => {return response.send(result)})
	// catch method will catch the error the find method will executed
	.catch(error => {return response.send(error)})
}

// Add task on our database

module.exports.createTask=(request, response)=>{
	const input = request.body;
	
	Task.findOne({name:input.name}).then(result => {
		if(result != null){
			return response.send("The task is already existing!");
		}else{
			let newTask = new Task({
				name:input.name
			});

			newTask.save().then(save =>{
				return response.send("The task is successfully added!")
			}).catch(error=>{
				return response.send(error);
			})
		}
	}).catch(error => {
		return response.send(error);
	})
};

// Controller that will delete the document that contains the given object

module.exports.deleteTask =(request, response)=>{
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - to find the document that contains the id and then delete the document

	Task.findByIdAndRemove(idToBeDeleted).then(result => { return response.send(result)}).catch(error => { return response.send(error)
	})
};

// Start of Activity Code

// controller that will the get the document with spcific id
module.exports.getById = (request, response) =>{

	let id = request.params.id;
	Task.findOne({_id:id}).then(result =>{
		return response.send(result)
	}).catch(error => {
		return response.send(`${id} cannot found!`)
	})
}

// Controller that will change the status of the task
module.exports.changeStatus=(request, response)=>{
	let id = request.params.id;
	let input = request.body;

	Task.findByIdAndUpdate(id, {status:input.status}).then(result =>{
		if(result.status === "complete"){
			return response.send(`The status is already complete!`)
		}else{
			return response.send(`Status changed to complete!`);
		}
	}).catch(error=>{
		return respond.send(error)
	})
} 

// End of Activity Code