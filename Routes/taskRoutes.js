const express = require("express")
const router = express.Router();

const taskControllers = require("../Controllers/taskControllers.js")


/*Routes*/

// route fo getAll
router.get("/", taskControllers.getAll);

// route for createTask
router.post("/addTask", taskControllers.createTask)

// Route for deleteTask
router.delete("/deleteTask/:id", taskControllers.deleteTask)

// Start of activity code

// route for the getById
router.get("/:id", taskControllers.getById)

// route for the changing of the status
router.put("/:id/complete", taskControllers.changeStatus)

// End of the activity code
module.exports = router;