const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./Routes/taskRoutes.js")

// Server
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//MongoDB Connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-villacampa.g4r4ic2.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
	// Allows us to avoid any current and future errors while connecting to mongodb
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

//Check connection
let db = mongoose.connection;

//Error catcher
db.on("error", console.error.bind(console, "Connection Error!"));

// Confirmation of the connection
db.once("open", ()=>console.log("We're now connected to the cloud!"))



app.use("/tasks", taskRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}!`))

// the connections
// model > controller > routes > server/application

/*
	Separation of concerns:
	1. model should be connected to the controller.
	2. Controller should
*/